<?php
/*
Plugin Name: Allow Hyphens and Underscores in Short URLs
Plugin URI: http://code.otto-hanika.de/allow_hyphen_underscore_in_url
Description: Allow hyphens and underscores in short URLs (like <tt>http://sho.rt/hello-world_foo-bar</tt>)
Version: 1.0
Author: Andreas Otto
Author URI: http://code.otto-hanika.de/
License: GPL 3.0 or later
*/

// No direct call
if( !defined( 'YOURLS_ABSPATH' ) ) die();

yourls_add_filter( 'get_shorturl_charset', 'allow_hyphen_underscore_in_url' );
function allow_hyphen_underscore_in_url( $in ) {
	return $in.'-_';
}
?>